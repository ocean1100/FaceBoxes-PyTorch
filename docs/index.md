# Introduction

2 个挑战：

1. 在杂乱背景下人脸视角大的变化需要人脸检测器精准的解决复杂人脸和非人脸的分类问题
1. 较大的搜索空间和人脸尺寸进一步增加了时间效率的需要

传统方法效率高但在人脸大的视角变化下精度不够，基于CNN的方法精度高但速度很慢。

受到 Faster R-CNN 的 RPN 以及 SSD 中多尺度机制的启发，便有了这篇可以在 CPU 上实时跑的 FaceBoxes。

## FaceBoxes

### (1) RDCL (Rapidly Digested Convolutional Layers，加速计算) {docsify-ignore}

* **缩小输入的空间大小：**为了快速减小输入的空间尺度大小，在卷积核池化上使用了一系列的大的 stride，在 Conv1, Pool1, Conv2, Pool2 上 stride 分别是 4, 2, 2, 2，RDCL 的 stride 一共是 32，意味着输入的尺度大小被快速减小了 32 倍。
* **选择合适的 kernel size：**一个网络开始的一些层的 kernel size 应该比较小以用来加速，同时也应该足够大用以减轻空间大小减小带来的信息损失。Conv1, Conv2 和所有的 Pool 分别选取 7\*7, 5\*5, 3\*3 的 kernel size。
* **减少输出通道数：**使用 C.ReLU 来减少输出通道数。

### (2) MSCL (Multiple Scale Convolutional Layers，丰富感受野，使不同层的 anchor 离散化以处理多尺度人脸) {docsify-ignore}

将 RPN 作为一个人脸检测器，不能获取很好的性能有以下两个原因：

1. RPN 中的 anchor 只和最后一个卷积层相关，其中的特征和分辨率在处理人脸变化上太弱
1. anchor 相应的层使用一系列不同的尺度来检测人脸，但只有单一的感受野，不能匹配不同尺度的人脸

为解决这个问题，对 MSCL 从以下两个角度去设计：

* **Multi-scale design along the dimension of network depth.** 如下图，anchor 在多尺度的 feature map 上面取，类似 SSD。

  ![](images/multi-scale-design-along-the-dimension-of-network-depth.png)

* **Multi-scale design along the dimension of network width.**使用inception模块，内部使用不同大小的卷积核，可以捕获到更多的尺度信息。

### (3) Anchor densification strategy {docsify-ignore}

Inception 的 anchor 尺度为 32\*32, 64\*64, 128\*128，Conv3\_2, Conv4\_2 的尺度分别为 256\*256 和 512\*512。

| Anchor Associated Layer | Default Anchor        | Receptive Field for Detection                                 |
| :---:                   | :---:                 | :---:                                                         |
| Inception3              | 32x32, 64x64, 128x128 | 143x143, 207x207, 271x271, 335x335, 399x399, 463x463, 527x527 |
| Conv3\_2                | 236x256               | 271x271, 335x335, 399x399, 463x463, 527x527, 591x591, 655x655 |
| Conv4\_2                | 512x512               | 527x527, 591x591, 655x655, 719x719, 783x783, 847x847, 911x911 |

anchor 的间隔和相应的层的 stride 相等。比如 Conv3_2 的 stride 是 64、anchor 大小为 256\*256，表示对应输入图片每 64 像素大小有一个 256\*256 的 anchor。anchor 密度为：

$$
    A_{\text{density}} = \frac{A_{\text{scale}}}{A_{\text{interval}}} \tag{*}
$$

$A_{\text{scale}}$ 表示 anchor 的尺度，$A_{\text{interval}}$ 表示 anchor 间隔。默认间隔分别设置为 32, 32, 32, 64。根据公式 $(*)$，对应的密度分别为 1, 2, 4, 4, 4。显然在不同尺度上 anchor 的密度不均衡。相比大的 anchor (128 - 512)，小的 anchor (32 和 64) 过于稀疏，将会导致在小脸检测中低的召回率。

为解决不均衡问题，此处提出新的 anchor 策略。为了加大一种 anchor 的密度，在一个感受野的中心均匀的堆叠 $n^2$ 个 anchor（本来是 1 个）用来预测。

<center>
    ![](images/examples-of-anchor-densification.png)
</center>

文章里对 32\*32 的 anchor 做了 4 倍，对 64\*64 的 anchor 做了 2 倍，这样就可以保证不同尺度的 anchor 有相同的密度。

## Training

**Training dataset**: WIDER FACE 的子集，12880 张图片。

**Data augmentation:**

* Color distorition: 根据[《Some Improvements on Deep Convolutional Neural Network Based Image Classification》](https://arxiv.org/ftp/arxiv/papers/1312/1312.5402.pdf)
* Random cropping: 从原图中随机裁剪 5 个方块 patch: 一个最大方块，其他的分别在范围 [0.3, 1] 之于原图尺寸
* Scale transformation: 将随机裁剪后的方块 patch 给 resize 到  1024\*1024
* Horizontal flipping: 0.5 的概率翻转
* Face-box filter: 如果 face box 的中心在处理后的图片上，则保持其重叠，然后将高或宽小于 20 像素的 face box 过滤出来

**Matching strategy:**

在训练时需要判断哪个 anchor 是和哪个 face bounding box 相关的。首先使用 jaccard overlap 将每个脸和 anchor 对应起来，然后对 anchor 和任意脸 jaccard overlap 高于阈值 (0.35) 的匹配起来。

**Loss function:**

和 Faster R-CNN 中的 RPN 用同样的 loss，一个 2 分类的 softmax loss 用来做分类，smooth L1 用来做回归。

**Hard negative mining:**

在 anchor 匹配后，大多数 anchor 都是负样本，导致正样本和负样本严重不均衡。为了更快更稳定的训练，将他们按照 loss 值排序并选取最高的几个，保证正样本和负样本的比例最高不超过 3:1。

**Other implementation details:**

Xavier 随机初始化。优化器 SGD，momentum: 0.9，weight decay: 5e-4，batch size: 32，迭代最大次数: 120k，初始 80k 迭代 learning rate:1e-3，80-100k迭代用 1e-4，100-120k 迭代用 1e-5，使用 caffe 实现。

## Experiments

<center>
  ![](images/comparation-experiments.png)
</center>

## Model analysis

FDDB 相比 AFW 和 PASCAL face 较为困难，因此这里在 FDDB 上作分析。

## Ablative Setting

1. 去掉 anchor densification strategy
1. 把 MSCL 替换为三层卷积，其大小都为 3\*3，输出数都和 MSCL 中前三个 Inception 的保持一致。同时，把 anchor 只和最后一层卷积关联
1. 把 RDCL 中的 C.ReLU 替换为 ReLU

<center>
  ![](images/ablative-Setting.png)
</center>

## Conclusion

- **Anchor densification strategy is crucial.**
- **MSCL is better.**
- **RDCL is efficient and accuracy-preserving.**

**实验结果：**

AFW:

<center>
    ![](images/experiment-result-afw.png)
</center>

PASCAL face:

<center>
    ![](images/experiment-result-pascal-face.png)
</center>

FDDB:

<center>
    ![](images/experiment-result-fddb.png)
</center>
