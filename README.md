# FaceBoxes-PyTorch

论文[《FaceBoxes: A CPU Real-time Face Detector with High Accuracy》](http://cn.arxiv.org/abs/1708.05234)的 PyTorch 复现。

[FaceBoxes-PyTorch 项目文档](http://ocean1100.gitee.io/faceboxes-pytorch)


#### 软件架构

软件架构说明
```flow
st=>start: Start
image_data=>operation: Image data
cond_train=>condition: Train
define_dataset=>operation: define our own dataset,
within implement __getitem__
and __len__ methods and transform

dataloader=>operation: Load DataLoader
get_data=>operation: 4D tensor-variable Cuda
ssd=>operation: Net()
backward=>operation: 1.Set iteration times
2.Calc loss
3.Backward
4.Record loss and draw map

detect=>operation: Detect
e=>end

st->image_data->cond_train
cond_train(yes)->define_dataset
define_dataset->dataloader
dataloader->get_data
get_data->ssd
ssd->backward
<!-- ssd->detect -->
cond_train(no)->ssd
```


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


